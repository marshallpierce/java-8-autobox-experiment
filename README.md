This [code](https://bitbucket.org/marshallpierce/java-8-autobox-experiment/src/master/src/main/java/org/mpierce/Foo.java?at=master) builds fine under Java 7. Under Java 8, I get:

```
src/main/java/org/mpierce/Foo.java:11: error: incomparable types: boolean and Object
        if (false == anObj()) {
                          ^
```
