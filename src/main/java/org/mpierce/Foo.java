package org.mpierce;

final class Foo {

    Object anObj() {
        return new Object();
    }

    void doSomething() {
        // this compiles under jdk 7 but not 8
        if (false == anObj()) {

        }
    }

}
